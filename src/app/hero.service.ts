import { Injectable } from '@angular/core';
import { Hero } from './hero';
// import { HEROES } from './mock-heroes';
import { Observable, of, ObservableLike } from 'rxjs';
import { HttpClient , HttpHeaders } from '@angular/common/http'; 
import { catchError, map, tap } from 'rxjs/operators';
import { timingSafeEqual } from 'crypto';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private heroesUrl = 'api/heroes'; //Url to web api
  public messages;

  constructor( private http: HttpClient ) { }

  getHeroes(): Observable<Hero[]> {
      return this.http.get<Hero[]>(this.heroesUrl)
                .pipe(
                  catchError(this.handleError('getHeroes',[]))
                );
  }
  
  getHero(id: number): Observable<Hero>{
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(url)
            .pipe(
                  catchError(this.handleError<Hero>(`getHero id=${id}`))
            );
  }

  updateHero(hero: Hero): Observable<any> {
    return this.http.put(this.heroesUrl, hero, httpOptions)
              .pipe(
                catchError(this.handleError<Hero>('updateHero id=${id}'))
              );
  }

  deleteHero(hero:Hero | number ): Observable<Hero>{
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;
    return this.http.delete<Hero>(url, httpOptions)
                .pipe(
                    catchError(this.handleError<Hero>('deleteHero'))
                );
  }
  
  addHero(hero : Hero): Observable<Hero> {
    return this.http.post<Hero>(this.heroesUrl, hero, httpOptions)
              .pipe(
                catchError(this.handleError<Hero>('addHero'))
              );
  }
  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

 /** Log a HeroService message with the MessageService */
 private log(message: string) {
  this.messages.add('HeroService: ' + message);
}

}
