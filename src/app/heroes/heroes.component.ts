import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  //properties 
  heroes: Hero[];
  selectedHero: Hero;
  message;


  // methods

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  		this.getHeroes();
  }


  getHeroes(): void {
      this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  delete(hero: Hero): void{
    this.heroService.deleteHero(hero)
        .subscribe();
  } 

  add(name: string): void {
    this.heroService.addHero({ name } as Hero)
        .subscribe(hero =>
          { 
            this.heroes.push(hero); 
          }
        );
  }
  
}
